public class solutions {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int k = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] != nums[k]) {
               
                k++;
                nums[k] = nums[i];
            }
        }

        return k + 1;
    }

    public static void main(String[] args) {
        solutions solution = new solutions();

        // Example 1
        int[] nums1 = {1, 1, 2};
        int result1 = solution.removeDuplicates(nums1);
        System.out.println(result1);  // Output: 2
        // Updated nums array: [1, 2, _, _, ...]

      
        int[] nums2 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int result2 = solution.removeDuplicates(nums2);
        System.out.println(result2);  // Output: 5
        // Updated nums array: [0, 1, 2, 3, 4, _, _, _, _, ...]
    }
}

