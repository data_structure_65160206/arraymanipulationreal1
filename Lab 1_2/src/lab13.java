import java.util.Arrays;

public class lab13 {
    public static void main(String[] args) {
        int[] num1 =  {1,2,3,0,0,0};
        int m = 3;
        int[] num2 = {2,5,6};
        int n = 3;
        merge(num1,m,num2,n);
        
    }
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] mergedAndSortedArray = new int[m + n];
        int k = 0;
        int l = 0;
        for(int i=0; i<(m+n); i++){
            if(k > (m-1)){
                mergedAndSortedArray[i] = nums2[l];
                l += 1;
            }
            else if(nums1[k] <= nums2[l]){
                mergedAndSortedArray[i] = nums1[k];
                k += 1;
            }
            else {
                mergedAndSortedArray[i] = nums2[l];
                l += 1;
            }
        }
    
       
        System.arraycopy(mergedAndSortedArray, 0, nums1, 0, m + n);
        System.out.println(Arrays.toString(mergedAndSortedArray));
    }
}
