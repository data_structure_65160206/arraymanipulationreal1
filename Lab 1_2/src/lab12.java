import java.util.Arrays;

public class lab12 {
    public static void main(String[] args) {
        int[] input = {1, 0, 2, 3, 0, 4, 5, 0};
        duplicateZeros(input);
       
        System.out.println(Arrays.toString(input));
    }

    public static void duplicateZeros(int[] arr) {
        int len = arr.length;
        if (len == 1)
            return;
        
        int start = 0, end = len - 1;
        while (start < end) {
            if (arr[start] == 0)
                end--;
            start++;
        }
        
        if (end == len - 1)
            return;
        
        for (int i = len - 1; i >= 0 && end >= 0; i--, end--) {
            arr[i] = arr[end];
            if (arr[end] == 0 && end != start) {
                arr[i - 1] = 0;
                i--;
            }
        }
    }
}
