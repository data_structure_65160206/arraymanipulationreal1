public class Lab31 {
    public int mySqrt(int x) {
        if (x == 0) {
            return 0;
        }

        long left = 1;
        long right = x;

        while (left <= right) {
            long mid = left + (right - left) / 2;
            long square = mid * mid;

            if (square == x) {
                return (int) mid;
            } else if (square < x) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }

        return (int) right;
    }

    public static void main(String[] args) {
        Lab31 solution = new Lab31();

        // Example 1
        int x1 = 4;
        int result1 = solution.mySqrt(x1);
        System.out.println(result1);  // Output: 2

        // Example 2
        int x2 = 8;
        int result2 = solution.mySqrt(x2);
        System.out.println(result2);  // Output: 2
    }
}
