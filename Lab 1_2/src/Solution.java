public class Solution {
    public int removeElement(int[] nums, int val) {
        
        int k = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
              
                k++;
            }
        }

      
        return k;
    }

    public static void main(String[] args) {
        Solution solution = new Solution();

        
        int[] nums1 = {3, 2, 2, 3};
        int val1 = 3;
        int result1 = solution.removeElement(nums1, val1);
        System.out.println(result1);  // Output: 2
        

        
        int[] nums2 = {0, 1, 2, 2, 3, 0, 4, 2};
        int val2 = 2;
        int result2 = solution.removeElement(nums2, val2);
        System.out.println(result2);  // Output: 5
       
}
}
