public class ArrayDeletionsLab {
    
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5};
        int indexToDelete = 2;
        int valueToDelete = 4;

        int[] updatedArray1 = deleteElementByIndex(array, indexToDelete);
        int[] updatedArray2 = deleteElementByValue(array, valueToDelete);
        
        for (int num : updatedArray1) {
            System.out.print(num + " ");
        }
        System.out.println();
        for (int num : updatedArray2) {
            System.out.print(num + " ");
        }
    }
    
    public static int[] deleteElementByIndex(int[] arr, int index) {
        if (index < 0 || index >= arr.length) {
            return arr;
        }

        for (int i = index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }

        int[] updatedArray = new int[arr.length - 1];
        System.arraycopy(arr, 0, updatedArray, 0, updatedArray.length);

        return updatedArray;
    }
    
    public static int[] deleteElementByValue(int[] arr, int value) {
        int indexToDelete = -1;
    
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                indexToDelete = i;
                break;
            }
        }
    
        if (indexToDelete == -1) {
            return arr;
        }
    
        int[] updatedArray = new int[arr.length - 1];
    
        // Copy elements before the indexToDelete
        System.arraycopy(arr, 0, updatedArray, 0, indexToDelete);
    
        // Copy elements after the indexToDelete
        System.arraycopy(arr, indexToDelete + 1, updatedArray, indexToDelete, arr.length - indexToDelete - 1);
    
        return updatedArray;
    }
    
    
    
}
