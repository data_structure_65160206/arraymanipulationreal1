import java.util.jar.Attributes.Name;

public class ArrayManipulation {
    public static void main(String[] args) throws Exception {
        int numbers[] = {5,8,3,2,7};
        String names[] = {"Alice", "Bob", "Charlie", "David"};
        double values[] = new double[4];
        // for loop print number array's
        for(int i = 0; i<numbers.length; i++){
            System.out.print(numbers[i]);
        }
        System.out.println();
        //initailized
        values[0] = 1.5;
        values[1] = 2.4;
        values[2] = 4.7;
        values[3] = 5.8;
        //for loop print name's array
        for(int i = 0; i<names.length; i++){
            System.out.print(names[i]+" ");
        }
        System.out.println();

        //Calculate and print the sum of all elements in the "numbers" array.
        int sum = 0;
        for(int i= 0; i<numbers.length; i++){
            sum = sum + numbers[i];
        }
        System.out.println("6.sum = "+sum);
        //Find and print the maximum value in the "values" array.
        double max = 0;
        for(int i= 0; i<values.length; i++){
            if(values[i]>max){
                max = values[i];
            }
        }
        System.out.println("max ="+max);
        
     
        String reversedNames[] = new String[names.length]; 
        int index = 0;
        for(int i=reversedNames.length-1  ;i>=0; i--){
            reversedNames[index] = names[i];
            index++;
        }
        for(int i = 0; i<names.length; i++){
            System.out.print(reversedNames[i]+" ");
        }
    }
}
